<?php 
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */ 
include("includes/config.php");

if(isset($_GET["id"])) {

   $post = new Posts();
   $user = new Users();

   // Get post info
   $p = $post->getPost($_GET["id"]);

   // Get and format authors name
   $name = $user->getUserName($p["author"]);
   $author = $name["name_first"] . " " . $name["name_last"];

   // Format Date
   $created = date_create($p["created"]);
   $date = date_format($created, "Y/m/d H:i" );
} else {
   header("Location: index.php");
}
?>
<?php
$page_title = $p['title'];
include("includes/header.php");
?>

<div class="wrap narrow">
   <?php 
   if ($p === NULL) { ?>
      <h1>Kunde inte hitta inlägget</h1>
      <p>Tyvärr finns det inget inlägg med detta ID.</p>
   <?php } else { ?>
      <h1><?= $page_title; ?></h1>
      <p class="meta">Upplagt <span><?= $date ?></span> av <?= $author ?></p>
      <?= $p['body']; ?>
   <?php } ?>
</div>

<?php
include("includes/footer.php");
?>