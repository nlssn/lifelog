<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");

if(!isset($_SESSION["id"])) { 
   header("Location: login.php?message=2"); 
}

if ( isset($_GET["id"]) && isset($_GET["delete"]) ) {
   $post = new Posts();

   $post->deletePost($_GET["id"]);
   header("Location: admin.php");
   exit;
}

$page_title = "Varning!";
include("includes/header.php");
?>

<div class="wrap narrow">
   <h1>Varning!</h1>
   <p>Du är på väg att radera ett inlägg. Är du säker på att du vill göra detta?</p>

   <a class="btn btn-delete" href="delete.php?id=<?= $_GET["id"]?>&delete">Ja, radera inlägget</a>
<a class="btn btn-ghost" href="editor.php?id=<?= $_GET["id"] ?>">Nej, jag ångrar mig</a>
</div>

<?php
include("includes/footer.php");
?>