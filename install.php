<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20
 * A simple installation script that creates the 
 * DB tables that are needed to run the Lifelog site.
 */

// Include config to get access to global variables
include("includes/config.php");

// Establish a connection to the database
$db = new mysqli(DBHOST, DBUSER, DBPASS, DBDATABASE);
if ($db->connect_errno > 0){
   die('Fel vid anslutning: ' . $db->connect_error);
}

// Drop existing tables to avoid errors
$sql  = "DROP TABLE IF EXISTS ll_users, ll_posts;";

// Set up users table
$sql .= "CREATE TABLE ll_users(
      id INT(11) PRIMARY KEY AUTO_INCREMENT,
      email VARCHAR(64) NOT NULL UNIQUE,
      name_first VARCHAR(32) NOT NULL,
      name_last VARCHAR(32) NOT NULL,
      password VARCHAR(128) NOT NULL,
      bio TEXT,
      registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
   );";

// Set up posts table
$sql .= "CREATE TABLE ll_posts(
      id INT(11) PRIMARY KEY AUTO_INCREMENT,
      author INT(11) REFERENCES ll_users(id),
      title VARCHAR(128) NOT NULL,
      body TEXT NOT NULL,
      summary VARCHAR(123) NOT NULL,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
   );";

echo "<pre>$sql</pre>";

// Make a query with the SQL instructions
if($db->multi_query($sql)) {
   echo "Databasen installerad.";
} else {
   echo "Fel vid installation av databas.";
}