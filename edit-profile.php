<?php include("includes/config.php"); ?>
<?php 
if(!isset($_SESSION["id"])) { 
   header("Location: login.php?message=2"); 
}

$user = new Users();
$id = $_SESSION["id"];

/* Because I prefill the fields with exisiting information,
 * the fields will always POST. If possible I would only want to
 * update the information that has actually changed.
 */
if(isset($_POST["password"])) {
   if($user->logInUser($_SESSION["email"], $_POST["password"])) {
      $email = $_POST["email"];
      $firstName = $_POST["firstname"];
      $lastName = $_POST["lastname"];
      $bio = $_POST["bio"];
   
      $user->setEmail($id, $email);
      $user->setFirstName($id, $firstName);
      $user->setLastName($id, $lastName);
      $user->setBio($id, $bio);

      // Make sure the new password acually has a length
      if(isset($_POST["newPassword"]) && strlen($_POST["newPassword"]) > 0) {
         $newPassword = $_POST["newPassword"];
         $user->setPassword($id, $newPassword);
      }

      $message = "<p class='success'>Ändringar sparade!</p>";
   } else {
      $message = "<p class='error'>Fel lösenord.</p>";
   } 
}

/* If an id is provided, and it matches the logged in user
 * go ahead and get the users info, so we can prefill the form
 */
if(isset($_GET["id"]) && $_GET["id"] === $_SESSION["id"]) {
   $profile = $user->getUser($_GET["id"]);
} else {
   header("Location: admin.php");
}

?>
<?php $page_title = "Redigera profil"; ?>
<script src="https://cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
<?php include("includes/header.php"); ?>

<div class="wrap narrow">
   <?php if(isset($message)) { echo $message; } ?>
   <h1><?= $page_title ?></h1>
   <form method="post" action="edit-profile.php?id=<?= $_GET["id"] ?>">
      <label for="email">Email / Användarnamn</label><br>
      <input type="text" id="email" name="email" value="<?= $profile["email"]; ?>"><br>
      <label for="firstname">Förnamn</label><br>
      <input type="text" id="firstname" name="firstname" value="<?= $profile["name_first"]; ?>"><br>
      <label for="lastname">Efternamn</label><br>
      <input type="text" id="lastname" name="lastname" value="<?= $profile["name_last"]; ?>"><br>
      <label for="bio">Biografi</label><br>
      <textarea name="bio" id="bio" cols="30" rows="10"><?php if(isset($profile["bio"])) { echo $profile["bio"]; } ?></textarea><br>
      <label for="newPassword">Byt lösenord</label><br>
      <input type="password" id="newPassword" name="newPassword"><br>
      <label for="password">Ditt nuvarande lösenord</label><br>
      <input type="password" id="password" name="password" required><br>
      <input type="submit" value="Spara" class="btn">
   </form>
</div>

<script>
   CKEDITOR.replace("bio");
</script>

<?php
include("includes/footer.php");
?>