<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */

include("includes/config.php");
$page_title = "Om oss";
include("includes/header.php");

$post = new Posts();
$user = new Users();
?>

<div class="wrap narrow">
   <h1><?= $page_title ?></h1>
   <p><b>Lifelog</b> är ett projektarbete av Johannes Nilsson. Det är tänkt att vara en blogg för personer som älskar träning och vill dela med sig av tips & tricks till nybörjare.</p>
   <p>Just nu finns det inga riktiga skribenter, men jag har satt upp några olika testkonton och fyllt bloggen med exempelmaterial.</p>
   <p>Kanske kan det bli en riktig bloggportal någon gång i framtiden.</p>
   <div class="registered-users">
      <h3>Våra skribenter</h3>
      <ul>
         <?php
         $usersArray = $user->getRegisteredUsers();

         foreach($usersArray as $u) {
         $name = $u["name_first"] . " " . $u["name_last"];
         $url = "profile.php?id=" . $u["id"];
         ?>
         <li><a href="<?= $url ?>"><?= $name ?></a></li>
         <?php
         }
         ?>
      </ul>
   </div>
</div>

<?php
include("includes/footer.php");
?>