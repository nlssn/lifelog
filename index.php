<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");
$page_title = "Hem";
include("includes/header.php");
?>

      <section class="presentation">
         <div class="wrap">
            <div class="text">
               <h2>Tillsammans blir vi starka.</h2>
               <p>Lifelog är ett bloggnätverk för oss som <em>älskar</em> att träna. I bloggen kan du läsa om tips &amp; tricks för att börja träna, samt personliga träningsloggar och historier från våra skribenter.
               </p>
               <a href="register.php" class="btn">Registrera dig</a> <a href="blog.php" class="btn btn-ghost">Läs bloggen</a>
            </div>
            <div class="image">
               <img src="assets/images/presentation.png" alt="Någon träningsbild">
            </div>
         </div>
      </section>
      <!-- ./presentation -->

      <section class="fp-2 wrap">
         <div class="featured-posts">
            <h2>Senaste inläggen</h2>
            <?php 
            $post = new Posts();
            $user = new Users();

            $posts = $post->getManyPosts(5, "desc");

            foreach($posts as $p) {
               // Get and format authors name
               $name = $user->getUserName($p["author"]);
               $author = $name["name_first"] . " " . $name["name_last"];

               // Format Date
               $created = date_create($p["created"]);
               $date = date_format($created, "Y/m/d H:i" );
            ?>
            
            <div class="featured-post">
               <a href="post.php?id=<?= $p["id"] ?>">
                  <h3><?= $p["title"] ?></h3>
                  <p class="meta">Upplagt <span><?= $date ?></span> av <?= $author ?></p>
                  <p><?= $p["summary"] ?></p>
                  <p class="read-more">Läs hela inlägget &xrarr;</p>
               </a>
            </div>

            <?php
            }
            ?>
         </div>
         <!-- /.featured-posts -->

         <div class="registered-users">
            <h3>Våra skribenter</h3>
            <ul>
               <?php
               $usersArray = $user->getRegisteredUsers();

               foreach($usersArray as $u) {
                  $name = $u["name_first"] . " " . $u["name_last"];
                  $url = "profile.php?id=" . $u["id"];
               ?>
               <li><a href="<?= $url ?>"><?= $name ?></a></li>
               <?php
               }
               ?>
            </ul>
         </div>
      </section>
      <!-- /.fp-2 -->

<?php
include("includes/footer.php");
?>