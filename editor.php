<?php 
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");

if(!isset($_SESSION["id"])) { 
   header("Location: login.php?message=2");
}

$post = new Posts();

// Check if there's a postID set. If no ID set editMode to false
if (isset($_GET["id"])) {
   $p = $post->getPost($_GET["id"]);

   // Make sure it's the posts author that's editing the post
   if ($p["author"] === $_SESSION["id"]) {
      $editMode = true;
      $page_title = "Redigera inlägg";
   } else {
      header("Location: admin.php?message=1");
   }
} else {
   $editMode = false;
   $page_title = "Nytt inlägg";
}

if(isset($_POST["title"])) {
   $title = $_POST["title"];
   $body = $_POST["body"];

   if(!$editMode) {
      if($post->addPost($_SESSION["id"],$title,$body)) {
         $message = "<p class='success'>Inlägget skapat!</p>";
      } else {
         $message = "<p class='error'>Något gick fel.</p>";
      }
   } else {
      if($post->updatePost($_GET["id"],$title,$body)) {
         $message = "<p class='success'>Inlägget uppdaterat!</p>";
      } else {
         $message = "<p class='error'>Något gick fel.</p>";
      }
   }
}
?>
<script src="https://cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
<?php include("includes/header.php"); ?>

<div class="wrap narrow">
   <h1><?= $page_title ?></h1>
   <?php if(isset($message)) { echo $message; } ?>

   <?php
   // Show a form for editing or creating a new posts, depending on if editMode is set to true or false
   if(!$editMode) { ?>
      <form method="post" action="editor.php">
         <label for="title">Inläggets titel</label><br>
         <input type="text" name="title" id="title" required><br>
         <label for="body">Innehåll</label><br>
         <textarea name="body" id="body" cols="30" rows="10" required></textarea><br>
         <input type="submit" value="Publicera" class="btn">
      </form>
   <?php
   } else {
      $title = $p["title"];
      $body = $p["body"]; ?>
      <form method="post" action="editor.php?id=<?= $_GET["id"] ?>">
         <label for="title">Inläggets titel</label><br>
         <input type="text" name="title" id="title" value="<?= $title ?>" required><br>
         <label for="body">Innehåll</label><br>
         <textarea name="body" id="body" cols="30" rows="10" required><?= $body ?></textarea><br>
         <input type="submit" value="Uppdatera inlägg" class="btn"> <a class="btn btn-delete" href="delete.php?id=<?= $_GET["id"] ?>">Radera</a>
      </form>
   <?php
   }
   ?>
</div>

<script>
   CKEDITOR.replace("body");
</script>

<?php
include("includes/footer.php");
?>