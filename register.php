<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");

if(isset($_POST["email"])) {
   $email = $_POST["email"];
   $password = $_POST["password"];
   $firstName = $_POST["fname"];
   $lastName = $_POST["lname"];

   $user = new Users();

   if($user->isEmailRegistered($email)) {
      $message = "<p class='error'>Ett konto har redan registrerats med den angivna emailadressen.</p>";
   } else {
      if($user->registerUser($email, $password, $firstName, $lastName)) {
         header("Location: login.php?message=3");
      } else {
         $message = "<p class='error'>Kunde inte skapa användare!</p>";
      }
   }

}
?>
<?php
$page_title = "Registrera dig";
include("includes/header.php");
?>

<div class="wrap">
   <form method="post" class="register-form">
      <h2>Registrera dig</h2>
      <?php if(isset($message)) { echo $message; } ?>
      <label for="fname">Förnamn</label><br>
      <input type="text" id="fname" name="fname" required><br>
      <label for="lname">Efternamn</label><br>
      <input type="text" id="lname" name="lname" required><br>
      <label for="email">Email</label><br>
      <input type="email" id="email" name="email" required><br>
      <label for="password">Lösenord</label><br>
      <input type="password" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Måste innehålla minst 8 tecken varav minst en siffra, en stor bokstav och en liten bokstav." required><br>
      <div id="password-requirements">
      <h4>Ditt lösenord måste innehålla:</h4>
         <p id="lower" class="invalid">En <b>liten</b> bokstav</p>
         <p id="upper" class="invalid">En <b>stor</b> bokstav</p>
         <p id="number" class="invalid">En <b>siffra</b></p>
         <p id="length" class="invalid">Minst <b>8 tecken</b></p>
      </div>
      <p><input type="checkbox" required name="tac"> Jag accepterar att ovanstående information sparas hos LifeLog</p>
      <input type="submit" value="Registera" class="btn">
   </form>
   <p class="account-check">Har du redan ett konto? <a href="login.php">Logga in här.</a></p>
</div>


<script src="assets/js/main.js"></script>

<?php
include("includes/footer.php");
?>