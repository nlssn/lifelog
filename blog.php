<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");
$page_title = "Bloggen";
include("includes/header.php");
?>
<div class="wrap">
   <div class="content">
   <h1><?= $page_title ?></h1>
   <div class="featured-posts">
   <?php 
      $post = new Posts();
      $user = new Users();

      $posts = $post->getManyPosts("ALL", "desc");

      foreach($posts as $p) {
         // Get and format authors name
         $name = $user->getUserName($p["author"]);
         $author = $name["name_first"] . " " . $name["name_last"];

         // Format Date
         $created = date_create($p["created"]);
         $date = date_format($created, "Y/m/d H:i" );
      ?>
      <div class="featured-post">
         <a href="post.php?id=<?= $p["id"] ?>">
         <h3><?= $p["title"] ?></h3>
         <p class="meta">Upplagt <span><?= $date ?></span> av <?= $author ?></p>
         <p><?= $p["summary"] ?></p>
         <p class="read-more">Läs hela inlägget &xrarr;</p>
         </a>
      </div>
      <?php
      }
      ?>
   </div>
   <!-- /.featured-posts -->
   </div>

   <div class="sidebar">
      <div>
         <h4>Sök</h4>
         <form>
            <input type="text" placeholder="Skriv något här...">
            <input class="btn btn-small btn-ghost" type="submit" value="Sök">
         </form>
      </div>
      <div>
         <h4>Mest lästa blogginlägg</h4>
         <ul>
            <li><a href="#0">Första inlägget</a> (9)</li>
            <li><a href="#0">Andra inlägget</a> (5)</li>
            <li><a href="#0">Tredje inlägget</a> (3)</li>
            <li><a href="#0">Fjärde inlägget</a> (2)</li>
            <li><a href="#0">Femte inlägget</a> (1)</li>
         </ul>
      </div>
      <div>
         <h4>Kategorier</h4>
         <ul>
            <li><a href="#0">Träning</a></li>
            <li><a href="#0">Kost</a></li>
            <li><a href="#0">Motivation</a></li>
            <li><a href="#0">Övrigt</a></li>
         </ul>       
      </div>
   </div>
</div>

<?php
include("includes/footer.php");
?>