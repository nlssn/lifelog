/* A simple  little script to visually show the user if 
 * their chosen password matches the set requirements.
 */

let reqs = document.getElementById("password-requirements");
let pwField = document.getElementById("password");
let lower = document.getElementById("lower");
let upper = document.getElementById("upper");
let number = document.getElementById("number");
let len = document.getElementById("length");

// Show requirements when password input is focused
pwField.onfocus = function() {
   reqs.style.display = "block";
};

// Hide requirements when focus is lost
pwField.onblur = function() {
   reqs.style.display = "none";
};

// When the user is writing in password input...
pwField.onkeyup = function() {
   // Validate lowercase letters
   let lowerCaseLetters = /[a-z]/g;
   if(pwField.value.match(lowerCaseLetters)) {
      lower.classList.remove("invalid");
      lower.classList.add("valid"); 
   } else {
      lower.classList.remove("valid");
      lower.classList.add("invalid"); 
   }

   // Validate uppercase letters
   let upperCaseLetters = /[A-Z]/g;
   if(pwField.value.match(upperCaseLetters)) {
      upper.classList.remove("invalid");
      upper.classList.add("valid"); 
   } else {
      upper.classList.remove("valid");
      upper.classList.add("invalid"); 
   }

   // Validate numbers
   let numbers = /[0-9]/g;
   if(pwField.value.match(numbers)) {
      number.classList.remove("invalid");
      number.classList.add("valid"); 
   } else {
      number.classList.remove("valid");
      number.classList.add("invalid"); 
   }

   // Validate length
   if(pwField.value.length >= 8) {
      len.classList.remove("invalid");
      len.classList.add("valid"); 
   } else {
      len.classList.remove("valid");
      len.classList.add("invalid"); 
   }
};