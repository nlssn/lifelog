<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */

class Posts {
   private $db;

   // Constructor
   function __construct() {
      $this->db = new mysqli(DBHOST, DBUSER, DBPASS, DBDATABASE);
      if($this->db->connect_errno > 0) {
         die("Fel vid anslutning till databas: " . $this->db->connect_error);
      }
   }

   // Add post
   public function addPost($author, $title, $body) {
      $author = $this->db->real_escape_string($author);
      $title = $this->db->real_escape_string($title);
      $body = $this->db->real_escape_string($body);
      $summary = substr(strip_tags($body), 0, 120) . '...'; // Strip HTML tags from summary

      $sql = "INSERT INTO ll_posts(author, title, body, summary) VALUES('$author', '$title', '$body', '$summary')";
   
      $result = $this->db->query($sql);
      return $result;
   }

   // Get specific post
   public function getPost($id) {
      $id = intval($id);

      $sql = "SELECT * FROM ll_posts WHERE id=$id";
      $result = $this->db->query($sql);
      $row = mysqli_fetch_array($result);

      return $row;
   }

   // Get all posts, or a limited set of posts, in specified order.
   public function getManyPosts($limit, $order) {
      if($limit !== "ALL") {
         $sql = "SELECT * FROM ll_posts ORDER BY created $order LIMIT $limit;";
      } else {
         $sql = "SELECT * FROM ll_posts ORDER BY created $order";
      }
      
      $result = $this->db->query($sql);

      return mysqli_fetch_all($result, MYSQLI_ASSOC);
   }

   // Get all posts from specified author
   public function getAuthorPosts($id) {
      $id = intval($id);

      $sql = "SELECT * FROM ll_posts WHERE author=$id ORDER BY created desc";
      $result = $this->db->query($sql);

      return mysqli_fetch_all($result, MYSQLI_ASSOC);
   }

   // Edit post
   public function updatePost($id, $title, $body) {
      $id = intval($id);
      $title = $this->db->real_escape_string($title);
      $body = $this->db->real_escape_string($body);
      $summary = substr($body, 0, 120).'...';

      $sql = "UPDATE ll_posts SET title='$title', body='$body', summary='$summary' WHERE id=$id;";
   
      $result = $this->db->query($sql);
      return $result;
   }

   // Remove post
   public function deletePost($id) {
      $id = intval($id);

      $sql = "DELETE FROM ll_posts WHERE id=$id";
      $result = $this->db->query($sql);

      return $result;
   }
}

