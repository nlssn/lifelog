<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */

class Users {
   private $db;
   private $email;
   private $password;
   private $firstName; // maybe I don't need this?
   private $lastName; // maybe I don't need this?

   function __construct() {
      $this->db = new mysqli(DBHOST, DBUSER, DBPASS, DBDATABASE);
      if($this->db->connect_errno > 0) {
         die("Fel vid anslutning till databas: " . $this->db->connect_error);
      }
   }

   // Register a new user
   public function registerUser($email, $password, $firstName, $lastName) {
      $email = $this->db->real_escape_string($email);
      $password = $this->db->real_escape_string($password);
      $firstName = $this->db->real_escape_string($firstName);
      $lastName = $this->db->real_escape_string($lastName);

      // Hash password
      $password = password_hash($password,PASSWORD_DEFAULT);

      $sql = "INSERT INTO ll_users(email,password,name_first, name_last) VALUES('$email', '$password', '$firstName', '$lastName')";
   
      $result = $this->db->query($sql);
      return $result;
   }

   // Log in existing user
   public function logInUser($email, $password) {
      $email = $this->db->real_escape_string($email);
      $password = $this->db->real_escape_string($password);

      $sql = "SELECT id,password,email FROM ll_users WHERE email='$email'";
      $result = $this->db->query($sql);

      if($result->num_rows > 0) {
         // Get the stored password from DB
         $row = $result->fetch_assoc();
         $storedPassword = $row["password"];

         // Check if entered password matches stored password
         if(password_verify($password, $storedPassword)) {
            $_SESSION["id"] = $row["id"];
            $_SESSION["email"] =  $row["email"];
            return true;
         } else {
            return false;
         }
      } else {
         return false; // if username doesn't exist.. false!
      }
   }

   // Check if a user exists with specified email
   public function isEmailRegistered($email) {
      $email = $this->db->real_escape_string($email);

      $sql = "SELECT email FROM ll_users WHERE email='$email'";
      $result = $this->db->query($sql);

      if($result->num_rows > 0) {
         return true;
      } else {
         return false;
      }
   }

   // Get a list of all registered users
   public function getRegisteredUsers() {
      $sql = "SELECT id, name_first, name_last FROM ll_users";
      $result = $this->db->query($sql);
      
      return mysqli_fetch_all($result, MYSQLI_ASSOC);
   }

   // Get all info about specified user
   public function getUser($id) {
      $id = intval($id);
      $sql = "SELECT * FROM ll_users WHERE id=$id";
      $result = $this->db->query($sql);
      $row = mysqli_fetch_array($result);
      return $row;
   }

   // Get a users name (first and last, as an assoc array)
   public function getUserName($id) {
      $id = intval($id);
      $sql = "SELECT name_first, name_last FROM ll_users WHERE id=$id";
      $result = $this->db->query($sql);
      $row = $result->fetch_assoc();
      return $row;
   }
   
   // Set email
   public function setEmail($id, $email) {
      $id = intval($id);
      $email = $this->db->real_escape_string($email);
      $sql = "UPDATE ll_users SET email='$email' WHERE id=$id";
      $result = $this->db->query($sql);
      return $result;
   }

   // Set password
   public function setPassword($id, $password) {
      $id = intval($id);
      $password = $this->db->real_escape_string($password);
      $password = password_hash($password,PASSWORD_DEFAULT); // Hash password
      $sql = "UPDATE ll_users SET password='$password' WHERE id=$id";
      $result = $this->db->query($sql);
      return $result;
   }

   // Set first name
   public function setFirstName($id, $firstName) {
      $id = intval($id);
      $firstName = $this->db->real_escape_string($firstName);
      $sql = "UPDATE ll_users SET name_first='$firstName' WHERE id=$id";
      $result = $this->db->query($sql);
      return $result;
   }

   // Set last name
   public function setLastName($id, $lastName) {
      $id = intval($id);
      $firstName = $this->db->real_escape_string($lastName);
      $sql = "UPDATE ll_users SET name_last='$lastName' WHERE id=$id";
      $result = $this->db->query($sql);
      return $result;
   }

   // Set biography
   public function setBio($id, $bio) {
      $id = intval($id);
      $firstName = $this->db->real_escape_string($bio);
      $sql = "UPDATE ll_users SET bio='$bio' WHERE id=$id";
      $result = $this->db->query($sql);
      return $result;
   }
}