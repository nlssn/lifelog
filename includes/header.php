<?php /* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */ ?>

<!DOCTYPE html>
<html lang="sv">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title><?= SITENAME . " | " . $page_title ?></title>
   <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
   <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
   <link rel="manifest" href="assets/images/favicons/site.webmanifest">
   <link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
   <link rel="shortcut icon" href="assets/images/favicons/favicon.ico">
   <meta name="msapplication-TileColor" content="#2b5797">
   <meta name="msapplication-config" content="assets/images/favicons/browserconfig.xml">
   <meta name="theme-color" content="#ffffff">
   <link href="https://fonts.googleapis.com/css?family=Dosis:400,500,700|Raleway:400,600&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="assets/css/normalize.css">
   <link rel="stylesheet" href="assets/css/style.css">
</head>
<body class="<?=basename($_SERVER["SCRIPT_FILENAME"], '.php')?>">
   <header class="site-header">
      <nav class="primary-navigation wrap">
         <a class="logo" href="index.php">
            <img src="assets/images/logo.png" alt="Lifelog">
         </a>
         <ul>
            <li><a href="index.php">Hem</a></li>
            <li><a href="blog.php">Bloggen</a></li>
            <li><a href="about.php">Om oss</a></li>
            <?php
               if(!isset($_SESSION["id"])) {
                  echo '<li><a href="login.php">Logga in</a></li>';
               } else {
                  echo '<li><a href="admin.php">Kontrollpanel</a></li>';
                  echo '<li><a href="logout.php">Logga ut</a></li>';
               }
            ?>
         </ul>
      </nav>
   </header>
   <!-- /.site-header -->

   <main class="page-content">