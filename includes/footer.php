   <?php /* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */ ?>
   
   </main>
   <!-- /.page-content -->

   <footer class="site-footer">
      <div class="grid wrap">
         <div class="grid-item">
            <h4>Om Lifelog</h4>
            <p>Vi som skriver den här bloggen älskar verkligen träning. Gör du också det? Häng med oss!</p>
         </div>
         <div class="grid-item">
            <h4>Kategorier</h4>
            <ul>
               <li><a href="#0">Träning</a></li>
               <li><a href="#0">Kost</a></li>
               <li><a href="#0">Motivation</a></li>
               <li><a href="#0">Övrigt</a></li>
            </ul>
         </div>
         <div class="grid-item">
            <h4>Guider</h4>
            <ul>
               <li><a href="#0">Styrka 101</a></li>
               <li><a href="#0">Träna hemma</a></li>
               <li><a href="#0">Sitt rätt</a></li>
               <li><a href="#0">Börja löpa</a></li>
            </ul>
         </div>
         <div class="grid-item">
            <h4>Nyhetsbrev</h4>
            <p>Få motiverande mail direkt i din inkorg!</p>
            <form>
               <input type="text" placeholder="din@epost.nu">
               <input type="button" value="Gå med" class="btn btn-small">
            </form>
         </div>
      </div>
   </footer>
   <!-- /.site-footer -->

</body>
</html>