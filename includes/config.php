<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */

// Start session
session_start();

/* Autoloading of classes */
spl_autoload_register(function ($class) {
   include 'classes/' . $class . '.class.php';
});

// Toggleable error reporting
$errors = 1;
if ($errors == 1) {
   error_reporting(-1); // Report all types of errors
   ini_set("display_errors", 1); // Display all errors
}

// Site specific constants
define("SITENAME", "Lifelog");

/* Local Database settings
define("DBHOST", "localhost");
define("DBUSER", "lifelog");
define("DBPASS", "1234");
define("DBDATABASE", "lifelog"); */

// MIUN database settings
define("DBHOST", "studentmysql.miun.se");
define("DBUSER", "joni1307");
define("DBPASS", "hn8bkvpi");
define("DBDATABASE", "joni1307");