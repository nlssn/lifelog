<?php
   /* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */

   session_start(); // Make sure the last active session is used
   session_destroy(); // Destroy that session
   header("Location: login.php?message=1"); // Redirect user to login page
   exit();
?>