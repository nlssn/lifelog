<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php");

if(isset($_GET["id"])) {
   $post = new Posts();
   $user = new Users();

   // Get user info & format it
   $info = $user->getUser($_GET["id"]);
   $author = $info["name_first"] . " " . $info["name_last"];
   
   if(isset($info["bio"])) {
      $bio = $info["bio"];
   } else {
      $bio = "<i>Den här personen har inte skrivit någon presentation ännu.</i>";
   }
   
} else {
   header("Location: index.php");
}
?>
<?php
$page_title = $author;
include("includes/header.php");
?>


<div class="wrap narrow">
   <h1><?= $page_title ?></h1>
   <div class="bio">
      <?= $bio ?>
   </div>
   <div class="authored-posts">
      <h3>Inlägg skrivna av <?= $author ?></h3>
      <ul>
         <?php
         $postsArray = $post->getAuthorPosts($_GET["id"]);

         foreach($postsArray as $p) {
            // Format Date
            $created = date_create($p["created"]);
            $date = date_format($created, "Y/m/d H:i" );
         ?>
         <li><a href="post.php?id=<?= $p["id"] ?>"><?= $p["title"] ?></a> (<?= $date ?>)</li>
         <?php
         }
         ?>
      </ul>
   </div>
</div>

<?php
include("includes/footer.php");
?>