<?php
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php"); 

if(isset($_GET["message"])) {
   $msg = $_GET["message"];

   switch($msg) {
      case 1:
         $message = "<p class='success'>Du är nu utloggad.</p>";
         break;
      case 2:
         $message = "<p class='error'>Inloggning krävs för att besöka denna sida.</p>";
         break;
      case 3:
         $message = "<p class='success'>Ditt konto är skapat! Du kan nu logga in.</p>";
         break;
   }
}

if(isset($_POST["username"])) {
   $username = $_POST["username"];
   $password = $_POST["password"];

   $user = new Users();

   if($user->logInUser($username,$password)) {
      header("Location: admin.php");
   } else {
      $message = "<p class='error'>Fel email eller lösenord!</p>";
   }
}

$page_title = "Logga in";
include("includes/header.php");
?>

<div class="wrap">
   <form method="post" class="login-form">
      <h2>Logga in</h2>
      <?php if(isset($message)) { echo $message; } ?>
      <label for="username">Användarnamn</label><br>
      <input type="text" id="username" name="username"><br>
      <label for="password">Lösenord</label><br>
      <input type="password" id="password" name="password"><br>
      <input type="submit" value="Logga in" class="btn">
   </form>
   <p class="account-check">Har du inget konto? <a href="register.php">Registrera dig här.</a></p>
</div>


<?php
include("includes/footer.php");
?>