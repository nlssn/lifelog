<?php 
/* Johannes Nilsson | DTG093 - Webbutveckling II | VT20 */
include("includes/config.php"); 
 
if(!isset($_SESSION["id"])) { 
   header("Location: login.php?message=2"); 
}

if(isset($_GET["message"])) {
   $msg = $_GET["message"];

   switch($msg) {
      case 1:
         $message = "<p class='error'>Inlägget du försökte redigera tillhör någon annan.</p>";
         break;
   }
}

$user = new Users();
$posts = new Posts();
?>
<?php $page_title = "Kontrollpanelen"; ?>
<?php include("includes/header.php"); ?>

<div class="wrap">
   <div class="content">
      <?php if(isset($message)) { echo $message; } ?>
      <h1><?= $page_title ?></h1>
      <p>Hej <?= $user->getUserName($_SESSION["id"])["name_first"]; ?>! Vad vill du göra idag?</p>
      <p><a href="editor.php">Skriva ett nytt inlägg</a> eller <a href="edit-profile.php?id=<?= $_SESSION["id"] ?>">Redigera din profil</a></p>
   </div>

   <div class="sidebar">
      <h3>Dina publicerade inlägg</h3>
      <p>Klicka på ett inlägg för att redigera det.</p>
      <ul>

      <?php
      $postItems = $posts->getAuthorPosts($_SESSION["id"]);

      foreach($postItems as $p) {
         // Format Date
         $created = date_create($p["created"]);
         $date = date_format($created, "Y/m/d H:i" );
      ?>
      <li><a href="editor.php?id=<?= $p["id"] ?>"><?= $p["title"] ?></a> (<?= $date ?>)</li>
      <?php
      }
      ?>
      </ul>
   </div>
</div>

<?php
include("includes/footer.php");
?>